//
//  WeatherAddLocationViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit
import Foundation

protocol WeatherAddLocationDelegate: class {
    func citiesDidChange()
}

class WeatherAddLocationViewController: RoutingViewController {

    @IBOutlet weak var closeButton: UIButton!

    weak var delegate: WeatherAddLocationDelegate?

    @IBAction func closeButtonWasPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    deinit {
        print("Deinit called for \(self)")
    }
}

extension WeatherAddLocationViewController: LocationProvidingViewDelegate {
    func presentAlertOverlay(with status: LocationStatus) {
        // stub
    }

    func findWeatherReportFailed(with error: Error) {
        print("ERROR: Finding weather report failed!")
    }

    func didFindWeatherReport() {
        DispatchQueue.main.async { [weak self] in
            self?.closeButton.setTitle("Save changes", for: .normal)
        }
        
        delegate?.citiesDidChange()
    }
}
