//
//  WeatherListTableViewCell.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

protocol WeatherListTableViewCellDelegate: class {
    func didPressSelectButton(with weatherReport: CityWeatherReport)
}

class WeatherListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!

    weak var delegate: WeatherListTableViewCellDelegate?

    private var currentWeatherReport: CityWeatherReport?

    func configure(with weatherReport: CityWeatherReport, editMode: Bool, selected: Bool) {
        if weatherReport.name == "" {
            nameLabel.text = "\(weatherReport.coordinate.lat) lat, \(weatherReport.coordinate.lon) lng"
        } else {
            nameLabel.text = weatherReport.name
        }

        descriptionLabel.text = weatherReport.weather.first?.description.capitalized ?? "No known current description"
        editButton.isSelected = selected
        currentWeatherReport = weatherReport
        configure(editMode: editMode)
    }

    public func configure(editMode: Bool) {
        editButton.isHidden = !editMode
    }

    @IBAction func editButtonPressed(_ sender: UIButton) {
        guard let weather = currentWeatherReport else {
            fatalError("FATAL ERROR: Edit button pressed on cell that does not contain a weather report!")
        }
        
        self.editButton.isSelected = !self.editButton.isSelected
        self.delegate?.didPressSelectButton(with: weather)
    }
}
