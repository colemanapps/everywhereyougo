//
//  WeatherViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class WeatherViewController: RoutingViewController {

    private var viewModel: WeatherViewModel!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addLocationAndDeleteButton: UIButton!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorLabel: UILabel!

    private var refreshingWeatherReportIdx: Int?

    var searchBarIsVisible: Bool {
        return searchBarBottomConstraint.constant == 0
    }
    
    var searchBarIsAnimating: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = WeatherViewModel(with: WeatherAPI())
        viewModel.weatherViewModelDelegate = self
        viewModel.delegate = self

        setUpView()
        setUpBarButtonItems()
        setUpSearchBar()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        hideSearchBar()
        searchBarIsAnimating = false
    }

    private func setUpView() {
        tableView.tableFooterView = UIView()
    }

    private func setUpBarButtonItems() {
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "cog")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(settingsTapped))
        navigationItem.leftBarButtonItem = leftBarButtonItem

        let firstRightBarButtonItem = UIBarButtonItem(image: UIImage(named: "search")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(searchTapped))
         let secondRightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editTapped))
        navigationItem.rightBarButtonItems = [firstRightBarButtonItem, secondRightBarButtonItem]
    }

    @objc func settingsTapped(){
        router.presentSettings(from: self)
    }

    @objc func searchTapped(){

        if !searchBarIsAnimating {
            if searchBarIsVisible {
                hideSearchBar()
            } else {
                showSearchBar()
            }
        }
    }

    @objc func editTapped(){

        // TODO: Make search and edit mode work well together
        if searchBarIsVisible {
            self.hideSearchBar()
        }
        viewModel.isEditMode = !viewModel.isEditMode
        setBottomBarButtons()
        navigationItem.rightBarButtonItems?.last?.title = viewModel.isEditMode ? "Cancel" : "Edit"
        tableView.reloadData()
    }

    @IBAction func addLocationAndDeletePressed(_ sender: UIButton) {

        if viewModel.isEditMode {
            User.shared.remove(with: viewModel.selectedReports)
            viewModel.updateDataSource()
            viewModel.selectedReports = []
            self.tableView.reloadData()
        } else {
            router.presentAddLocation(from: self)
        }
    }

    @IBAction func selectAllButtonPressed(_ sender: UIButton) {

        self.setSelectButton(isSelected: !selectAllButton.isSelected)

        if selectAllButton.isSelected {
            viewModel.selectedReports.append(contentsOf: viewModel.dataSourceArray.map({ return $0.name}))

            // remove duplicates
            viewModel.selectedReports = Array(Set(viewModel.selectedReports))

        } else {
            viewModel.selectedReports.removeAll()
        }
    }

    func setBottomBarButtons() {
        self.setSelectButton(isSelected: selectAllButton.isSelected)
        self.setAddLocationAndDeleteButton()
    }

    func setSelectButton(isSelected: Bool) {
        selectAllButton.isSelected = isSelected
        selectAllButton.isHidden = !self.viewModel.isEditMode

        if isSelected {
            selectAllButton.setTitle("Deselect all", for: .normal)
        } else {
            selectAllButton.setTitle("Select all", for: .normal)
        }
    }

    func setAddLocationAndDeleteButton() {
        addLocationAndDeleteButton.isEnabled = self.viewModel.isEditMode ? viewModel.selectedReports.count > 0 : true
        addLocationAndDeleteButton.setTitle(self.viewModel.isEditMode ? "Delete" : "Add new location", for: .normal)
    }

    deinit {
        print("Deinit called for \(self)")
    }
}

extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherListTableViewCell.reuseIdentifier(), for: indexPath) as! WeatherListTableViewCell
        cell.delegate = self

        let weatherReport = viewModel.dataSourceArray[indexPath.row]
        cell.configure(with: weatherReport, editMode: self.viewModel.isEditMode, selected: self.viewModel.selectedReports.contains(weatherReport.name))
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if !self.viewModel.isEditMode {
            let weatherReport = viewModel.dataSourceArray[indexPath.row]

            // if the data is more than an hour old, refresh the data
            if viewModel.dataIsMoreThanHourOld(fetchTime: weatherReport.fetchTime) {
                viewModel.fetchWeather(with: weatherReport.coordinate)
                refreshingWeatherReportIdx = indexPath.row
            } else {
                router.pushLocationDetail(from: self, with: weatherReport)
            }

        } else {

            if let cell = tableView.cellForRow(at: indexPath) as? WeatherListTableViewCell {
                cell.editButton.sendActions(for: .touchUpInside)
            }
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension WeatherViewController: WeatherListTableViewCellDelegate {

    func didPressSelectButton(with weatherReport: CityWeatherReport) {
        if viewModel.selectedReports.contains(weatherReport.name) {
            if let index = viewModel.selectedReports.firstIndex(of: weatherReport.name) {
                viewModel.selectedReports.remove(at: index)
            }
        } else {
            viewModel.selectedReports.append(weatherReport.name)
        }
    }
}


extension WeatherViewController: WeatherAddLocationDelegate {
    func citiesDidChange() {
        viewModel.updateDataSource()
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }

            self.errorLabel.isHidden = self.viewModel.dataSourceIsEmpty() ? true : false
            self.navigationItem.rightBarButtonItems?.forEach({ (item) in
                item.isEnabled = self.viewModel.dataSourceIsEmpty() ? true : false
            })
            self.tableView.reloadData()
        }
    }
}

// MARK: Show/hide search bar methods
extension WeatherViewController {

    func showSearchBar() {
        guard !viewModel.isEditMode else { return }

        searchBarBottomConstraint.constant = 0

        searchBarIsAnimating = true
        searchBar.becomeFirstResponder()

        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()

        }) { (isComplete) in
            if isComplete {
                self.searchBarIsAnimating = false
            }
        }
    }

    func hideSearchBar() {

        // reset
        searchBar.text = ""
        viewModel.updateDataSource()
        tableView.reloadData()

        searchBarBottomConstraint.constant = searchBar.frame.size.height

        searchBarIsAnimating = true
        dismissKeyboard()

        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        }) { (isComplete) in
            if isComplete {
                self.searchBarIsAnimating = false
            }
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        // stop scrolling when user clicked on the search button mid-scroll
        guard !searchBarIsAnimating else {
            scrollView.setContentOffset(scrollView.contentOffset, animated: false)
            return
        }

        let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)

        // if scrolling down, hide the bar
        if (actualPosition.y <= 0){
            if searchBarIsVisible {
                hideSearchBar()
            }
        }
    }
}

// MARK: Searchbar methods
extension WeatherViewController: UISearchBarDelegate {

    private func setUpSearchBar() {

        // Stop searching when tapping on the view
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(WeatherViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)

        searchBar.delegate = self
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.startSearch(with: searchText)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension WeatherViewController: WeatherViewModelDelegate {
    func datasourceIsEmpty() {
        navigationItem.rightBarButtonItems?.forEach({ (item) in
            item.isEnabled = false
        })

        viewModel.isEditMode = false
        setBottomBarButtons()

        setErrorLabel(with: "There don't seem to be any locations saved. Add one by clicking the button on the bottom of this screen!")
    }

    func selectedReportsWasFilled() {
        addLocationAndDeleteButton.isEnabled = true
        if viewModel.allReports.count == viewModel.selectedReports.count {
            setSelectButton(isSelected: true)
            tableView.reloadData()
        } else {
            setSelectButton(isSelected: false)
        }
    }

    func selectedReportsWasEmptied() {
        addLocationAndDeleteButton.isEnabled = viewModel.isEditMode ? false : true
        setSelectButton(isSelected: false)
        selectAllButton.isSelected = false
        tableView.reloadData()
    }

    func searchSucceeded() {
        errorLabel.isHidden = true
        tableView.reloadData()
    }

    func searchFailed(with query: String) {
        setErrorLabel(with: "Sorry! We couldn't find any results for \(query). Change your query and try again!")
        tableView.reloadData()
    }

    func setErrorLabel(with text: String) {
        errorLabel.isHidden = false
        errorLabel.text = text
    }
}

extension WeatherViewController: LocationViewModelDelegate {
    func stateChanged(with state: State) {
        switch state {

        case .loading:
            print("Should be loading")
        case .results:
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.viewModel.updateDataSource()
                if let idx = self.refreshingWeatherReportIdx {
                    let weatherReport = self.viewModel.dataSourceArray[idx]
                    self.router.pushLocationDetail(from: self, with: weatherReport)
                }
            }
        case .error(_):
            print("ERROR: Refreshing the data failed! Swallow the error for now.")
        }
    }
}

extension WeatherViewController: SettingsViewControllerDelegate {
    func unitsWereChanged() {
        viewModel.updateDataSource()
    }
}
