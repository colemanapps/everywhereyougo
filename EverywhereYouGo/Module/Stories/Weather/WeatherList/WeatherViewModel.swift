//
//  WeatherViewModel.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

protocol WeatherViewModelContract {
    func startSearch(with query: String)
    func dataIsMoreThanHourOld(fetchTime: Date) -> Bool
}

protocol WeatherViewModelDelegate: class {
    func searchSucceeded()
    func searchFailed(with query: String)
    func selectedReportsWasFilled()
    func selectedReportsWasEmptied()
    func datasourceIsEmpty()
}

class WeatherViewModel: LocationViewModel, WeatherViewModelContract {

    var dataSourceArray: [CityWeatherReport]!

    var allReports: [CityWeatherReport] {
        return User.shared.weatherReports
    }

    var selectedReports: [String] = [] {
        didSet {
            selectedReports.count > 0 ? weatherViewModelDelegate?.selectedReportsWasFilled() : weatherViewModelDelegate?.selectedReportsWasEmptied()
        }
    }

    var isEditMode: Bool = false
    
    weak var weatherViewModelDelegate: WeatherViewModelDelegate?

    override init(with weatherAPI: WeatherAPIContract) {
        super.init(with: weatherAPI)
        updateDataSource()
    }

    func updateDataSource() {
        dataSourceArray = allReports

        if dataSourceArray.count == 0 {
            weatherViewModelDelegate?.datasourceIsEmpty()
        }
    }
}

extension WeatherViewModel {

    func numberOfSections() -> Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        return dataSourceArray.count
    }
}

extension WeatherViewModel {

    func startSearch(with query: String) {

        if query == "" {
            dataSourceArray = allReports
        } else {
            dataSourceArray = allReports.filter {
                $0.name.range(of: query, options: .caseInsensitive) != nil
            }
        }

        if dataSourceArray.count > 0 {
            weatherViewModelDelegate?.searchSucceeded()
        } else {
            weatherViewModelDelegate?.searchFailed(with: query)
        }
    }

    func dataSourceIsEmpty() -> Bool {
        return dataSourceArray.count > 0
    }
}

extension WeatherViewModel {

    func dataIsMoreThanHourOld(fetchTime: Date) -> Bool {
        if let diff = Calendar.current.dateComponents([.hour], from: fetchTime, to: Date()).hour, diff > 1 {
            return true
        } else {
            return false
        }
    }

}
