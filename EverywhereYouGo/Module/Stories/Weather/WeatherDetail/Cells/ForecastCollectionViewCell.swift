//
//  ForecastCollectionViewCell.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!

    func configure(with weatherReport: CityWeatherReport?) {

        if let temp = weatherReport?.main.temp.roundToNearestHalf() {
            tempLabel.text = "\(temp)°"
        } else {
            tempLabel.text = "Forecast coming soon!"
        }

        if let icon = weatherReport?.weather.first?.icon {
            startDownloadingImage(with: icon)
        }
    }

    func startDownloadingImage(with icon: String) {
        let urlString = URLBuilder.buildWeatherIconURL(with: icon)
        if let url = URL(string: urlString) {
            imageView.download(from: url)
        }
    }
}
