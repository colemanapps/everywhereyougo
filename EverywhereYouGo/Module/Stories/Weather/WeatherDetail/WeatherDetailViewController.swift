//
//  WeatherDetailViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import UIKit

class WeatherDetailViewController: RoutingViewController {

    private let COLLECTION_HEIGHT_NORMAL: CGFloat = 120
    private let COLLECTION_HEIGHT_COMPACT: CGFloat = 80
    private let IMAGEVIEW_HEIGHT_NORMAL: CGFloat = 100
    private let IMAGEVIEW_HEIGHT_COMPACT: CGFloat = 0
    private let BOTTOM_CONSTRAINT_NORMAL: CGFloat = 80
    private let BOTTOM_CONSTRAINT_COMPACT: CGFloat = 8

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempContainer: UIView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humidityContainer: UIView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var rainContainer: UIView!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var windContainer: UIView!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var fiveDayForecastLabel: UILabel!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    // Used by forecast
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private var viewModel: WeatherDetailViewModel!

    var cityWeatherReport: CityWeatherReport!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = WeatherDetailViewModel(with: WeatherAPI())
        viewModel.delegate = self
        startDownloadingImage()
        startFetchingForecast()
        loadLayout()
    }

    override func willMove(toParent parent: UIViewController?) {
        if parent == nil {
            navigationController?.navigationBar.barTintColor = UIColor(named: "Orange")
        }
    }

    func loadLayout() {
        if cityWeatherReport.name == "" {
            textLabel.text = "\(cityWeatherReport.coordinate.lat) lat, \(cityWeatherReport.coordinate.lon) lng"
        } else {
            textLabel.text = cityWeatherReport.name
        }
        
        descriptionLabel.text = cityWeatherReport.weather.first?.description.capitalized ?? "No known current description"

        tempLabel.text = "\(cityWeatherReport.main.temp.roundToNearestHalf())° (min: \(cityWeatherReport.main.temp_min.roundToNearestHalf())°, max: \(cityWeatherReport.main.temp_max.roundToNearestHalf())°)"
        humidityLabel.text = "\(cityWeatherReport.main.humidity)"

        if let wind = cityWeatherReport.wind {
            var windText = "\(wind.speed)"
            if let windDeg = wind.deg {
                windText = windText + " (∠ \(windDeg))"
            }

            windLabel.text = windText

        } else {
            windContainer.isHidden = true
        }

        if let rain = cityWeatherReport.rain {
            rainLabel.text = rain.printableRain
        } else {
            rainContainer.isHidden = true
        }
    }

    func startDownloadingImage() {

        if let icon = cityWeatherReport.weather.first?.icon {
            let urlString = URLBuilder.buildWeatherIconURL(with: icon)
            if let url = URL(string: urlString) {
                weatherImageView.download(from: url)
            }
        } else {
            addPlaceholderImage()
        }
    }

    func startFetchingForecast() {
        viewModel.fetchFiveDayForecast(with: cityWeatherReport.coordinate)
    }

    func addPlaceholderImage() {
        weatherImageView.image = UIImage(named: "sun")
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {


        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in


            switch newCollection.verticalSizeClass {
            case .compact:
                self.collectionViewHeightConstraint.constant = self.COLLECTION_HEIGHT_COMPACT
                self.imageViewHeight.constant = self.IMAGEVIEW_HEIGHT_COMPACT
                self.weatherImageView.isHidden = true
                self.bottomConstraint.constant = self.BOTTOM_CONSTRAINT_COMPACT
            default:
                self.collectionViewHeightConstraint.constant = self.COLLECTION_HEIGHT_NORMAL
                self.imageViewHeight.constant = self.IMAGEVIEW_HEIGHT_NORMAL
                self.weatherImageView.isHidden = false
                self.bottomConstraint.constant = self.BOTTOM_CONSTRAINT_NORMAL
            }

            self.collectionView.collectionViewLayout.invalidateLayout()

        })

        super.willTransition(to: newCollection, with: coordinator)
    }

    deinit {
        print("Deinit called for \(self)")
    }
}

extension WeatherDetailViewController: WeatherDetailViewModelDelegate {
    func stateChanged(with state: State) {
        DispatchQueue.main.async { [weak self] in
            switch state {
            case .results:
                self?.activityIndicator.isHidden = true
                self?.collectionView.reloadData()
            default:
                // if something went wrong for some reason, hide the five day forecast label
                self?.fiveDayForecastLabel.isHidden = true
                self?.activityIndicator.isHidden = true
                self?.collectionView.isHidden = true
            }
        }
    }
}

extension WeatherDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        if UIDevice.current.orientation == .portrait {
            return CGSize(width: (self.view.frame.width - 32 - 12) / 5, height: self.collectionViewHeightConstraint.constant)
        } else {
            return CGSize(width: (self.view.frame.width - 100) / 5, height: self.collectionViewHeightConstraint.constant)
        }
    }
}

extension WeatherDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems(in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ForecastCollectionViewCell.reuseIdentifier(), for: indexPath) as! ForecastCollectionViewCell

        let weatherReport = viewModel.dataSource[safe: indexPath.row]
        cell.configure(with: weatherReport)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
}


