//
//  WeatherDetailViewModel.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

protocol WeatherDetailViewModelContract {
    func fetchFiveDayForecast(with coordinate: Coord)
}

protocol WeatherDetailViewModelDelegate: class {
    func stateChanged(with state: State)
}

class WeatherDetailViewModel: WeatherDetailViewModelContract {

    var dataSource: [CityWeatherReport] = []

    weak var delegate: WeatherDetailViewModelDelegate?

    var state: State = State.loading {
        didSet {
            delegate?.stateChanged(with: self.state)
        }
    }

    var api: WeatherAPIContract

    init(with api: WeatherAPIContract) {
        self.api = api
    }

    func fetchFiveDayForecast(with coordinate: Coord) {

        api.fetchFiveDayForecast(with: coordinate) { [weak self] (result) in

            switch result {
            case .success(let forecast):

                if let fiveDayReport = forecast.fiveDayReport {
                    self?.dataSource = fiveDayReport
                    self?.state = State.results
                } else {
                    self?.state = State.error(GenericAPIError.parsingFailed)
                }

            case .failure(let error):
                self?.state = State.error(error)
            }
        }
    }
}

extension WeatherDetailViewModel {

    func numberOfSections() -> Int {
        return 1
    }

    func numberOfItems(in section: Int) -> Int {
        if self.state == State.loading {
            return 0
        } else {
            return 5
        }
    }
}
