//
//  SettingsViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import UIKit

enum Unit: String {
    case Imperial = "imperial"
    case Metric = "metric"
}

protocol SettingsViewControllerDelegate: class {
    func unitsWereChanged()
}

class SettingsViewController: RoutingViewController {

    @IBOutlet weak var unitSwitch: UISwitch!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    weak var delegate: SettingsViewControllerDelegate?

    private let dispatchGroup = DispatchGroup()

    private var viewModel: LocationViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = LocationViewModel(with: WeatherAPI())
        viewModel.delegate = self

        if User.shared.unit == .Metric {
            unitSwitch.isOn = true
        } else {
            unitSwitch.isOn = false
        }
    }

    @IBAction func metricToggleChanged(_ sender: UISwitch) {
        if sender.isOn {
            User.shared.unit = .Metric
        } else {
            User.shared.unit = .Imperial
        }

        refetchItems()
    }

    @IBAction func closeButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    func refetchItems() {

        activityIndicator.isHidden = false

        unitSwitch.isEnabled = false
        closeButton.isEnabled = false

        User.shared.weatherReports.forEach { (weatherReport) in
            dispatchGroup.enter()
            viewModel.fetchWeather(with: weatherReport.coordinate)
        }

        dispatchGroup.notify(queue: .main) { [weak self] in
            self?.activityIndicator.isHidden = true
            self?.unitSwitch.isEnabled = true
            self?.closeButton.isEnabled = true
            self?.delegate?.unitsWereChanged()
        }
    }

    deinit {
        print("Deinit called for \(self)")
    }
}

extension SettingsViewController: LocationViewModelDelegate {
    func stateChanged(with state: State) {
        switch state {
        default:
            dispatchGroup.leave()
        }
    }
}
