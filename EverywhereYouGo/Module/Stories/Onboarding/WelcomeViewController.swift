//
//  WelcomeViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class WelcomeViewController: RoutingViewController {

    // constants
    private let SUN_HEIGHT_NORMAL: CGFloat = 265
    private let SUN_HEIGHT_COMPACT: CGFloat = 120

    // outlets
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var sunImageView: UIImageView!
    @IBOutlet weak var sunHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
    }

    func setupButton() {
        getStartedButton.layer.cornerRadius = Constants.CORNER_RADIUS
    }

    @IBAction func getStartedButtonWasPressed(_ sender: UIButton) {

        UIView.animate(withDuration: 1.5, delay: .zero, options: .curveEaseInOut, animations: {
            self.sunImageView.transform = self.sunImageView.transform.rotated(by: CGFloat(Double.pi))
        }) { (isCompleted) in
            if isCompleted {
                self.router.pushOnboardingInitialSetup(from: self.navigationController!)
            }
        }
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {

        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in

            switch newCollection.verticalSizeClass {
            case .compact:
                self.sunHeightConstraint.constant = self.SUN_HEIGHT_COMPACT
            default:
                self.sunHeightConstraint.constant = self.SUN_HEIGHT_NORMAL
            }

        })

        super.willTransition(to: newCollection, with: coordinator)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    deinit {
        print("Deinit called for \(self)")
    }
}
