//
//  InitialSetupViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit
import MapKit

class InitialSetupViewController: RoutingViewController {
    deinit {
        print("Deinit called for \(self)")
    }
}

extension InitialSetupViewController: LocationProvidingViewDelegate {
    func didFindWeatherReport() {
        DispatchQueue.main.async {
            self.router.presentWeather()
        }
    }

    func findWeatherReportFailed(with error: Error) {
        print("API called failed!!!!")
    }

    func presentAlertOverlay(with status: LocationStatus) {
        router.presentRequestLocationOverlay(with: status, over: self)
    }


}

extension InitialSetupViewController: LocationPermissionOverlayViewControllerDelegate {
    func locationPermissionWasChanged() {
        print("CHANGED")
    }
}
