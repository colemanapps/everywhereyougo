//
//  DismissBlurAnimatedTransitioning.swift
//  DottCodingChallenge
//
//  Created by George Coleman on 22/02/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class DismissBlurAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        let fromView = transitionContext.view(forKey: .from)!
        
        let blurEffectView = container.viewWithTag(123) as? UIVisualEffectView
        
        let animator = UIViewPropertyAnimator(duration: 0.5, curve: .linear) {
            
            fromView.alpha = 0
            blurEffectView?.effect = nil
        }
        
        animator.addCompletion { _ in
            
            transitionContext.completeTransition(true)
        }
        
        animator.startAnimation()
    }
}
