//
//  Constants.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class Constants {
    static let CORNER_RADIUS: CGFloat = 12
    static let SAVED_WEATHER_REPORTS_KEY: String = "SavedWeatherReports"
    static let SAVED_UNITS_KEY: String = "SavedUnits"
}
