//
//  PresentBlurAnimatedTransitioning.swift
//  DottCodingChallenge
//
//  Created by George Coleman on 22/02/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class PresentBlurAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        
        let blurEffectView = UIVisualEffectView()
        blurEffectView.tag = 123
        blurEffectView.frame = container.bounds
        container.addSubview(blurEffectView)
        
        toView.alpha = 0
        toView.frame = container.bounds
        container.addSubview(toView)
        
        let animator = UIViewPropertyAnimator(duration: 0.5, curve: .linear) {
            
            toView.alpha = 1
            blurEffectView.effect = UIBlurEffect(style: .dark)
        }
        
        animator.addCompletion { _ in
            
            transitionContext.completeTransition(true)
        }
        
        animator.startAnimation()
    }
}
