//
//  State.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

enum State: Equatable {

    case loading
    case results
    case error(Error)

    static func == (lhs: State, rhs: State)
        -> Bool {
            switch (lhs, rhs) {
            case (.loading, .loading): return true
            case (.results, .results): return true
            case let (.error(l), .error(r)): return (l as NSError).code == (r as NSError).code
            default: return false
            }
    }
}
