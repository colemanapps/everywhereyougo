//
//  MapAnnotationProvider.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import MapKit

protocol MapAnnotationProviderContract {
    func createMapAnnotations(for weatherReports: [CityWeatherReport]) -> [MKPointAnnotation]
    func removeMapAnnotations(for weatherReport: CityWeatherReport) -> [MKPointAnnotation]
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation, with disclosureButton: UIButton) -> MKAnnotationView?
    func setCurrentlySelectedAnnotation(annotation: MKAnnotation?)
    func getUniqueAnnotations(from annotationArray: [MKPointAnnotation]) -> [MKPointAnnotation]
}

class MapAnnotationProvider: MapAnnotationProviderContract {

    private let ANNOTATION_ID = "AnnotationIdentifier"
    private let ICON_NAME = "sun"

    var currentlySelectedAnnotation: MKAnnotation?

    var currentAnnotationsArray: [MKPointAnnotation] = []

    func createMapAnnotations(for weatherReports: [CityWeatherReport]) -> [MKPointAnnotation] {

        var annotationsArray: [MKPointAnnotation] = []

        for report in weatherReports {
            let annotation = MKPointAnnotation()
            annotation.title = report.name
            annotation.coordinate = CLLocationCoordinate2D(latitude: report.coordinate.lat, longitude: report.coordinate.lon)
            annotationsArray.append(annotation)
        }

        let uniqueValues = getUniqueAnnotations(from: annotationsArray)

        currentAnnotationsArray = annotationsArray

        return uniqueValues
    }

    func removeMapAnnotations(for weatherReport: CityWeatherReport) -> [MKPointAnnotation] {

        currentAnnotationsArray.removeAll(where: { $0.coordinate.latitude == weatherReport.coordinate.lat && $0.coordinate.longitude == weatherReport.coordinate.lon })

        return currentAnnotationsArray
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation, with disclosureButton: UIButton) -> MKAnnotationView? {

        var annotationView: MKAnnotationView?

        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: ANNOTATION_ID) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: ANNOTATION_ID)
            annotationView?.rightCalloutAccessoryView = disclosureButton
        }

        if let annotationView = annotationView {

            if let image = UIImage(named: ICON_NAME)?.resize(with: 28) {
                annotationView.image = image
            }
            annotationView.canShowCallout = true
        }

        return annotationView
    }

    func setCurrentlySelectedAnnotation(annotation: MKAnnotation?) {

        currentlySelectedAnnotation = annotation
    }

    func getUniqueAnnotations(from annotationArray: [MKPointAnnotation]) -> [MKPointAnnotation] {

        var tempArray = annotationArray
        var indexesToRemove: [Int] = []
        for (idx, item) in annotationArray.enumerated() {
            for annotation in currentAnnotationsArray {
                if (annotation.coordinate.latitude == item.coordinate.latitude) && (annotation.coordinate.longitude == item.coordinate.longitude) {
                    indexesToRemove.append(idx)
                }
            }
        }

        tempArray.remove(at: indexesToRemove)
        return tempArray
    }
}

