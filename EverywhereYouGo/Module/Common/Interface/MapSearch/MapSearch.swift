//
//  MapSearch.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit
import MapKit

@IBDesignable class MapSearch: LocationProvidingView {

    override var xibName: String {
        return "MapSearch"
    }

    @IBOutlet weak var mapView: MKMapView!
    private var viewModel: MapViewModel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        viewModel = MapViewModel(with: MapAnnotationProvider(), and: WeatherAPI())
        viewModel.delegate = self

        mapView.delegate = self

        setupMapView()

        let annotations = self.viewModel.createMapAnnotations(for: User.shared.weatherReports)
        mapView.showAnnotations(annotations, animated: true)
    }

    func setupMapView() {
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.chooseCity(sender:)))
        mapView.addGestureRecognizer(longPressGesture)
    }

    @objc func chooseCity(sender: UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizer.State.began { return }
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)

        let coordinate = Coord(lat: locationCoordinate.latitude, lon: locationCoordinate.longitude)
        viewModel.locationAreaFound(with: coordinate)
    }

    override func handleResults() {
        super.handleResults()
        let annotations = self.viewModel.createMapAnnotations(for: User.shared.weatherReports)
        DispatchQueue.main.async { [weak self] in
            self?.mapView.addAnnotations(annotations)
        }
    }
}

extension MapSearch: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named: "remove"), for: .normal)
        button.addTarget(self, action: #selector(removeCity), for: .touchUpInside)

        return viewModel.annotationProvider.mapView(mapView, viewFor: annotation, with: button)
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        viewModel.annotationProvider.setCurrentlySelectedAnnotation(annotation: view.annotation)
    }

    @objc func removeCity() {
        if let selectedAnnotation = viewModel.annotationProvider.currentlySelectedAnnotation {
            mapView.removeAnnotation(selectedAnnotation)
        }
        viewModel.removeCity()
        delegate?.didFindWeatherReport()
    }

}
