//
//  MapViewModel.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import MapKit

protocol MapViewModelContract {
    init(with annotationProvider: MapAnnotationProvider, and api: WeatherAPIContract, and user: UserContract)
    func createMapAnnotations(for weatherReports: [CityWeatherReport]) -> [MKPointAnnotation]
    func removeCity()
}

class MapViewModel: LocationViewModel, MapViewModelContract {

    var annotationProvider: MapAnnotationProvider
    var user: UserContract

    required init(with annotationProvider: MapAnnotationProvider, and api: WeatherAPIContract, and user: UserContract = User.shared) {
        self.annotationProvider = annotationProvider
        self.user = user
        super.init(with: api)
    }

    func createMapAnnotations(for weatherReports: [CityWeatherReport]) -> [MKPointAnnotation] {
        return annotationProvider.createMapAnnotations(for: weatherReports)
    }

    func removeCity() {
        if let coordinate = annotationProvider.currentlySelectedAnnotation?.coordinate {
            user.remove(at: Coord(lat: coordinate.latitude, lon: coordinate.longitude))
        }
    }
}
