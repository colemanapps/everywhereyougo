//
//  LocationProvidingView.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

@objc protocol LocationProvidingViewDelegate: class {
    func didFindWeatherReport()
    func findWeatherReportFailed(with error: Error)
    func presentAlertOverlay(with status: LocationStatus)
}

class LocationProvidingView: LoadableXib {

    @IBOutlet weak var delegate: LocationProvidingViewDelegate?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromNib()
    }
}

extension LocationProvidingView: LocationViewModelDelegate {
    func stateChanged(with state: State) {
        switch state {

        case .loading:
            print("Should be loading")
        case .results:
            self.handleResults()
        case .error(let error):
            self.handleErrors(with: error)
        }
    }
}

extension LocationProvidingView {
    @objc func handleResults() {
        delegate?.didFindWeatherReport()
    }

    func handleErrors(with error: Error) {
        delegate?.findWeatherReportFailed(with: error)
    }
}
 
