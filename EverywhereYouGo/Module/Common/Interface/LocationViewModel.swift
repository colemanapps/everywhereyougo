//
//  LocationViewModel.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

protocol LocationViewModelContract {
    func fetchWeather(with coordinate: Coord)
    func locationAreaFound(with coordinate: Coord)
    var dataSource: CityWeatherReport! { get set }
    var delegate: LocationViewModelDelegate? { get set }
}

protocol LocationViewModelDelegate: class {
    func stateChanged(with state: State)
}

class LocationViewModel: LocationViewModelContract {

    var api: WeatherAPIContract
    weak var delegate: LocationViewModelDelegate?

    var dataSource: CityWeatherReport! {
        didSet {
            User.shared.addWeatherReport(with: self.dataSource)
        }
    }

    init(with api: WeatherAPIContract) {
        self.api = api
    }

    var state: State = State.loading {
        didSet {
            delegate?.stateChanged(with: state)
        }
    }

    func fetchWeather(with coordinate: Coord) {

        api.fetchWeather(with: coordinate) { [weak self] (result) in

            switch result {
            case .success(let weatherReport):

                self?.dataSource = weatherReport
                self?.state = State.results

            case .failure(let error):
                self?.state = State.error(error)
            }
        }
    }
}

extension LocationViewModel: UserLocationDelegate {

    func locationAreaFound(with coordinate: Coord) {
        fetchWeather(with: coordinate)
    }
}
