//
//  CurrentLocationViewModel.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import MapKit

protocol CurrentLocationViewModelContract {
    func getEnabledStatus() -> Bool
    func getLocationStatus(for authStatus: CLAuthorizationStatus) -> LocationStatus
    func getLocationStatusForDisabledServices() -> LocationStatus
    func requestUserLocation()
    var locationService: UserLocationContract { get set }
}

class CurrentLocationViewModel: LocationViewModel, CurrentLocationViewModelContract {

    var locationService: UserLocationContract

    init(with locationService: UserLocationContract, and api: WeatherAPIContract) {
        self.locationService = locationService

        super.init(with: api)

        self.locationService.delegate = self
    }

    func getEnabledStatus() -> Bool {
        return locationService.getEnabledStatus()
    }

    func getLocationStatus(for authStatus: CLAuthorizationStatus) -> LocationStatus {
        return locationService.getLocationStatus(for: authStatus)
    }

    func getLocationStatusForDisabledServices() -> LocationStatus {
        return locationService.getLocationStatusForDisabledServices()
    }
}

extension CurrentLocationViewModel {

    func requestUserLocation() {
        locationService.requestUserLocation()
    }
}
