//
//  UserLocationContract.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import MapKit

@objc enum LocationStatus: Int {
    case Granted
    case NotDetermined
    case NoAccess
}

enum UserLocationError: Error {
    case noLocationFound
}

protocol UserLocationContract {
    func getLocationStatusForDisabledServices() -> LocationStatus
    func getLocationStatus(for authStatus: CLAuthorizationStatus) -> LocationStatus
    func getEnabledStatus() -> Bool
    func requestUserLocation()

    var foundLocation: Coord? { get set }
    var delegate: UserLocationDelegate? { get set }
}

protocol UserLocationDelegate: class {
    func locationAreaFound(with coordinate: Coord)
}

extension UserLocationContract {

    func getLocationStatus(for authStatus: CLAuthorizationStatus) -> LocationStatus {

        switch authStatus {
        case .notDetermined:
            return .NotDetermined
        case .restricted, .denied:
            return .NoAccess
        case .authorizedAlways, .authorizedWhenInUse:
            return .Granted
        @unknown default:
            fatalError("FATAL ERROR: Unknown authorisation status!")
        }
    }

    func getLocationStatusForDisabledServices() -> LocationStatus {
        return .NoAccess
    }

    func getEnabledStatus() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
}


class UserLocationService: NSObject, UserLocationContract {

    weak var delegate: UserLocationDelegate?

    var foundLocation: Coord? {
        didSet {
            guard let coordinate = foundLocation else { fatalError("FATAL ERROR: Found location didSet called with nil, should not happen!") }
            delegate?.locationAreaFound(with: coordinate)
        }
    }

    fileprivate var locationManager = CLLocationManager()

    override init() {
        super.init()
        locationManager.delegate = self
    }

    func requestUserLocation() {
        locationManager.requestLocation()
    }
}

extension UserLocationService: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        manager.stopUpdatingLocation()
        if let location = locations.last {
            foundLocation = Coord(lat: location.coordinate.latitude, lon: location.coordinate.longitude)
        } else {
            fatalError("FATAL ERROR: Did update locations, without actually finding a location!")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
}
