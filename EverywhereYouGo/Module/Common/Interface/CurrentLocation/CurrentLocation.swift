//
//  CurrentLocation.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit
import MapKit

@IBDesignable class CurrentLocation: LocationProvidingView {

    override var xibName: String {
        return "CurrentLocation"
    }

    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var buttonContainerView: UIView!
    
    private var viewModel: CurrentLocationViewModel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromNib()
        setupButton()

        viewModel = CurrentLocationViewModel(with: UserLocationService(), and: WeatherAPI())
        viewModel.delegate = self
    }

    func setupButton() {
        currentLocationButton.layer.cornerRadius = Constants.CORNER_RADIUS
    }

    @IBAction func currentLocationButtonPressed(_ sender: UIButton) {
        handleLocationPermission()
    }

    func handleLocationPermission() {

        let status = getLocationStatus()
        if status == .Granted {

            viewModel?.requestUserLocation()

        } else {
            self.delegate?.presentAlertOverlay(with: status)
        }
    }

    func getLocationStatus() -> LocationStatus {
        if viewModel.getEnabledStatus() {
            return viewModel.getLocationStatus(for: CLLocationManager.authorizationStatus())
        } else {
            return viewModel.getLocationStatusForDisabledServices()
        }
    }
}


