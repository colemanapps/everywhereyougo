//
//  LocationUndeterminedOverlayViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 22/02/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit
import CoreLocation

class LocationUndeterminedOverlayViewController: LocationPermissionOverlayViewController {
    
    private var locationManager: CLLocationManager!

    override func loadLayout() {
        
        super.loadLayout()
        
        locationManager = CLLocationManager()
    }
    
    override func configureSubtitle() {
        
        super.configureSubtitle()
        subtitle.text = "To use the following application, we will need to know your location."
    }
    
    override func configureOKButton() {
        
        super.configureOKButton()
        okButton.setTitle("Get started!", for: .normal)
        
    }
    
    override func okButtonPressed(_ sender: UIButton) {
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
    }
}

extension LocationUndeterminedOverlayViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        dismiss(animated: true, completion: nil)
        delegate?.locationPermissionWasChanged()
    }
}
