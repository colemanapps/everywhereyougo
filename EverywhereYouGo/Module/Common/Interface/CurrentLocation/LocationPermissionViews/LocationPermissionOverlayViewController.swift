//
//  LocationPermissionOverlayViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 22/02/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

protocol LocationPermissionOverlayViewControllerDelegate: class {
    
    func locationPermissionWasChanged()
}

class LocationPermissionOverlayViewController: UIViewController {
    
    weak var delegate: LocationPermissionOverlayViewControllerDelegate?

    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var firstStepButton: UIButton!
    @IBOutlet weak var secondStepLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    
    @IBOutlet weak var settingsIcon: UIImageView!
    @IBOutlet weak var topIcon: UIImageView!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadLayout()
    }
    
    func loadLayout() {
        
        setAlphaOfElementsTo0()
        
        configureTopIcon()
        configureTitle()
        configureSubtitle()
        configureOKButton()
        
        UIView.animate(withDuration: 0.3) {
            self.setAlphaOfElementsTo1()
        }
    }
    
    func configureTopIcon() {
        
        topIcon.isHidden = false
        topIcon.tintColor = .white
    }
    
    func configureTitle() {
        
        alertTitle.font = UIFont.systemFont(ofSize: 24)
        alertTitle.text = "Ah oh! There's no location available.."
    }
    
    func configureSubtitle() {
        
        subtitle.font = UIFont.systemFont(ofSize: 18)
    }
    
    func configureOKButton() {
    
        okButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        okButton.backgroundColor = .clear
        okButton.layer.cornerRadius = 8
        okButton.layer.borderWidth = 1
        okButton.layer.borderColor = UIColor.white.cgColor
        okButton.isHidden = false
    }
    
    func setAlphaOfElementsTo0() {
        
        topIcon.alpha = 0
        alertTitle.alpha = 0
        subtitle.alpha = 0
        containerView.alpha = 0
        okButton.alpha = 0
    }
    
    func setAlphaOfElementsTo1() {

        topIcon.alpha = 1
        alertTitle.alpha = 1
        subtitle.alpha = 1
        containerView.alpha = 1
        okButton.alpha = 1
    }
    
    @IBAction func didPressOpenSettings(_ sender: UIButton) {
        
        guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(settingsURL, completionHandler: nil)
    }
    
    @IBAction func okButtonPressed(_ sender: UIButton) {
        // stub
    }
}


extension LocationPermissionOverlayViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return PresentBlurAnimatedTransitioning()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissBlurAnimatedTransitioning()
    }
}

