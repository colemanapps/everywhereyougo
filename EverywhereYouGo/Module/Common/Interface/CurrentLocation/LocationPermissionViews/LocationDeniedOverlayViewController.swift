//
//  LocationDeniedOverlayViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 22/02/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

class LocationDeniedOverlayViewController: LocationPermissionOverlayViewController {
    
    override func loadLayout() {
        
        super.loadLayout()
        
        containerView.isHidden = false
        
        configureFirstButton()
        configureSecondLabel()
    }
    
    override func configureSubtitle() {
        
        super.configureSubtitle()
        subtitle.text = "It seems as if your location was disabled. Bummer! Follow the following steps to re-enable location services."
    }
    
    func configureFirstButton() {
        
        let text = "Click here to go to the Settings app"
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        firstStepButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        firstStepButton.setAttributedTitle(attributedText, for: .normal)
        
        firstStepButton.contentHorizontalAlignment = .leading
        firstStepButton.titleLabel?.lineBreakMode = .byWordWrapping
        firstStepButton.titleLabel?.numberOfLines = 2
        
    }
    
    func configureSecondLabel() {
        
        secondStepLabel.font = UIFont.systemFont(ofSize: 16)
        secondStepLabel.text = "Click on this icon, and select 'Always' or 'While Using the App'"
    }
    
    override func configureOKButton() {
        
        super.configureOKButton()
        okButton.setTitle("Done!", for: .normal)

    }
    
    override func setAlphaOfElementsTo0() {
        
        super.setAlphaOfElementsTo0()
        containerView.alpha = 0
    }
    
    override func setAlphaOfElementsTo1() {
        
        super.setAlphaOfElementsTo1()
        containerView.alpha = 1
    }
    
    override func okButtonPressed(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        delegate?.locationPermissionWasChanged()
    }
}
