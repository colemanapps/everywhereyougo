//
//  UIImageView.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

extension UIImageView {

    func download(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {

        contentMode = mode

        let urlRequest = URLRequest(url: url)

        API().perform(request: urlRequest) { (result) in
            switch result {
            case .success(let data):
                guard
                    let image = UIImage(data: data) else {
                        print("ERROR: Data returned from API was not an image, swallowing the error. No need to show the user.")
                        return
                }
                DispatchQueue.main.async() {
                    self.image = image
                }
            case .failure(_):
                print("ERROR: Getting data for image failed, but swallowing the error. No need to show the user.")
            }
        }
    }
}
