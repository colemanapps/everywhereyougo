//
//  UIViewController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

extension UIViewController {
    static var storyboardID: String {
        return "\(self)"
    }

    static func create(from storyboard: Storyboard) -> RoutingViewController {
        let routingViewController = storyboard.instance.instantiateViewController(withIdentifier: storyboardID) as! RoutingViewController
        return routingViewController
    }
}
