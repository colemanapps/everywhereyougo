//
//  UITableViewCell.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

extension UITableViewCell {

    class func reuseIdentifier() -> String {
        return "\(self)"
    }

}
