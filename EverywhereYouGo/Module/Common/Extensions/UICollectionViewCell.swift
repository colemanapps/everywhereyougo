//
//  UICollectionViewCell.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

extension UICollectionViewCell {

    class func reuseIdentifier() -> String {
        return "\(self)"
    }

}
