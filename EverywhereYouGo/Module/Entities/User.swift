//
//  User.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

protocol UserContract {
    var weatherReports: [CityWeatherReport] { get set }
    func remove(at coord: Coord)
}

class User: UserContract {

    static let shared = User()

    private var dataWasReadFromDisc = false
    private var unitWasReadFromDisc = false

    var weatherReports: [CityWeatherReport] = [] {
        didSet {
            writeReportsToDisc()
        }
    }

    var unit: Unit = Unit.Metric {
        didSet {
            writeUnitsToDisc()
        }
    }

    private init() {}

    // if the weather report already exists, it's a refresh. Replace it with the newer version.
    func addWeatherReport(with cityWeatherReport: CityWeatherReport) {
        
        var valueWasAdded = false
        for (idx, report) in weatherReports.enumerated() {
            if report.coordinate.lat == cityWeatherReport.coordinate.lat && report.coordinate.lon == cityWeatherReport.coordinate.lon {
                weatherReports[idx] = cityWeatherReport
                valueWasAdded = true
            }
        }
        
        // if the value wasn't added in the for loop it's a new entry, append it!
        if !valueWasAdded {

            // For now, don't add locations with the same name.
            // TODO: Make this a little bit smarter to be sure that it's not vastly different cities that happen to share a name in another country for example
            guard !weatherReports.contains(where: { $0.name == cityWeatherReport.name }) else { return }
            weatherReports.append(cityWeatherReport)
        }
    }

    func writeReportsToDisc() {

        guard dataWasReadFromDisc == false else {
            dataWasReadFromDisc = false
            return
        }

        do {
            let encoded = try JSONEncoder().encode(weatherReports)
            UserDefaults.standard.set(encoded, forKey: Constants.SAVED_WEATHER_REPORTS_KEY)
        } catch {
            fatalError("ERROR: Encoding weather reports failed!")
        }
    }

    func readReportsFromDisc() -> Bool {
        if let savedWeatherReports = UserDefaults.standard.object(forKey: Constants.SAVED_WEATHER_REPORTS_KEY) as? Data {
            do {
                let decodedWeatherReports = try JSONDecoder().decode([CityWeatherReport].self, from: savedWeatherReports)
                dataWasReadFromDisc = true
                weatherReports = decodedWeatherReports
                return weatherReports.count > 0 ? true : false
            } catch {
                fatalError("ERROR: Decoding weather reports failed!")
            }
        }

        return false
    }

    func readUnitsFromDisc() {
        if let savedUnits = UserDefaults.standard.string(forKey: Constants.SAVED_UNITS_KEY) {
            if let unit = Unit(rawValue: savedUnits) {
                unitWasReadFromDisc = true
                self.unit = unit
            } else {
                fatalError("ERROR: An unknown unit type was saved to disk!")
            }
        }
    }

    func writeUnitsToDisc() {

        guard unitWasReadFromDisc == false else {
            unitWasReadFromDisc = false
            return
        }

        UserDefaults.standard.set(self.unit.rawValue, forKey: Constants.SAVED_UNITS_KEY)
    }

    func remove(at coord: Coord) {
        let weatherReportArray = self.weatherReports.filter({ !($0.coordinate.lat == coord.lat && $0.coordinate.lon == coord.lon) })
        self.weatherReports = weatherReportArray
    }

    func remove(with names: [String]) {
        let weatherReportArray = self.weatherReports.filter({ !(names.contains($0.name)) })
        self.weatherReports = weatherReportArray
    }
}
