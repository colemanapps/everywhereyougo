//
//  Rain.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct Rain: Codable {
    let oneHour: Double?
    let threeHour: Double?
    var printableRain: String = ""

    enum CodingKeys: String, CodingKey {
        case oneHour = "1h"
        case threeHour = "3h"
        case printableRain = "printableRain"
    }

    init(from decoder: Decoder) throws {
        let weatherContainer = try decoder.container(keyedBy: CodingKeys.self)

        let hour = try weatherContainer.decodeIfPresent(Double.self, forKey: .oneHour)
        let hour3 = try weatherContainer.decodeIfPresent(Double.self, forKey: .threeHour)

        oneHour = hour
        threeHour = hour3

        if hour3 != nil || hour != nil {
            if let threeHour = hour3 {
                printableRain = "\(threeHour) mm in the last 3 hours"
            } else if let oneHour = hour {
                printableRain = "\(oneHour) mm in the last hour"
            }
        } else {
            printableRain = "No rain expected!"
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(oneHour, forKey: .oneHour)
        try container.encodeIfPresent(threeHour, forKey: .threeHour)
        try container.encode(printableRain, forKey: .printableRain)
    }
}
