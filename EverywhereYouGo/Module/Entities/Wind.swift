//
//  Wind.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct Wind: Codable {

    enum CodingKeys: CodingKey { case speed, deg }

    let speed: Double
    let deg: Double?

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(speed, forKey: .speed)
        try container.encodeIfPresent(deg, forKey: .deg)
    }
}
