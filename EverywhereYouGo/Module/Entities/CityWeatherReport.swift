//
//  CityWeatherReport.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct CityWeatherReport: Codable {

    let weather: [DescriptiveWeather]
    let rain: Rain?
    let name: String
    let wind: Wind?
    let coordinate: Coord
    let main: Main
    let forecast: [CityWeatherReport]?
    let fetchTime: Date
    let dt_txt: String?

    enum CodingKeys: CodingKey { case weather, rain, wind, name, main, coord, fetchTime, forecast, dt_txt }

    init(from decoder: Decoder) throws {
        let weatherContainer = try decoder.container(keyedBy: CodingKeys.self)
        try name =  weatherContainer.decodeIfPresent(String.self, forKey: .name) ?? "No name found"
        try coordinate = weatherContainer.decodeIfPresent(Coord.self, forKey: .coord) ?? Coord(lat: 0, lon: 0)
        try weather = weatherContainer.decode([DescriptiveWeather].self, forKey: .weather)
        try main = weatherContainer.decode(Main.self, forKey: .main)
        try wind = weatherContainer.decodeIfPresent(Wind.self, forKey: .wind)
        try rain = weatherContainer.decodeIfPresent(Rain.self, forKey: .rain)
        try forecast = weatherContainer.decodeIfPresent([CityWeatherReport].self, forKey: .forecast)
        try dt_txt =  weatherContainer.decodeIfPresent(String.self, forKey: .dt_txt)

        // needed to refresh data if out of date
        try fetchTime = weatherContainer.decodeIfPresent(Date.self, forKey: .fetchTime) ?? Date()
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encodeIfPresent(wind, forKey: .wind)
        try container.encodeIfPresent(rain, forKey: .rain)
        try container.encode(weather, forKey: .weather)
        try container.encode(coordinate, forKey: .coord)
        try container.encode(main, forKey: .main)
        try container.encode(fetchTime, forKey: .fetchTime)
        try container.encode(forecast, forKey: .forecast)
        try container.encode(dt_txt, forKey: .dt_txt)
    }
}
