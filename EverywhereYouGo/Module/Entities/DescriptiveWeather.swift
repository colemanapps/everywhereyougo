//
//  DescriptiveWeather.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 19/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct DescriptiveWeather: Codable {

    let description: String
    let icon: String

    enum CodingKeys: CodingKey { case description, icon }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(description, forKey: .description)
        try container.encode(icon, forKey: .icon)
    }
}
