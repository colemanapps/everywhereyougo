//
//  Forecast.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 20/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

struct Forecast: Decodable {
    let hourlyList: [CityWeatherReport]
    var fiveDayReport: [CityWeatherReport]?

    enum CodingKeys: String, CodingKey {
        case hourlyList = "list"
        case fiveDayReport = "fiveDayReport"
    }

    init(from decoder: Decoder) throws {
        let weatherContainer = try decoder.container(keyedBy: CodingKeys.self)

        try hourlyList =  weatherContainer.decode([CityWeatherReport].self, forKey: .hourlyList)
        let daysReport = try weatherContainer.decodeIfPresent([CityWeatherReport].self, forKey: .fiveDayReport)

        fiveDayReport = formatFiveDayReportArray(from: daysReport)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(hourlyList, forKey: .hourlyList)
        try container.encode(fiveDayReport, forKey: .fiveDayReport)
    }

    func formatFiveDayReportArray(from daysReport: [CityWeatherReport]?) -> [CityWeatherReport]? {

        var fiveDayReportArray: [CityWeatherReport] = []
        if let fiveDays = daysReport {
            fiveDayReportArray = fiveDays
        } else {
            hourlyList.forEach { (report) in
                if let dateText = report.dt_txt {

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"

                    for i in 1...5 {
                        if let date = Calendar.current.date(byAdding: .day, value: i, to: Date()) {
                            dateFormatter.string(from: date)

                            // always fetch the weather at noon, except if you're making the call before noon, the last day get the latest possible time point from the api

                            var hour: Int = 12
                            if i == 5 {
                                let calendar = Calendar.current
                                let comp = calendar.dateComponents([.hour], from: date)
                                if let compHour = comp.hour, compHour < 12 {
                                    hour = compHour.roundToThree()
                                }
                            }

                            if dateText == "\(dateFormatter.string(from: date)) \(hour):00:00" {
                                fiveDayReportArray.append(report)
                            }
                        }
                    }
                }
            }
        }

        return fiveDayReportArray
    }

}
