//
//  WeatherAPIInterface.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import UIKit

protocol WeatherAPIContract {
    func fetchWeather(with coordinate: Coord, withCompletion completion: @escaping (Result<CityWeatherReport, GenericAPIError>) -> ())
    func fetchFiveDayForecast(with coordinate: Coord, withCompletion completion: @escaping (Result<Forecast, GenericAPIError>) -> ())
}

extension WeatherAPIContract where Self: API {

    func constructCityWeatherReport(from data: Data) -> Result<CityWeatherReport, GenericAPIError> {
        do {
            let parsedJSON = try JSONDecoder().decode(CityWeatherReport.self, from: data)
            return checkResponseValue(parsedJSON)
        } catch {
            fatalError("FATAL ERROR: Parsing failed! Check the response JSON!")
        }
    }

    func constructCityWeatherForecast(from data: Data) -> Result<Forecast, GenericAPIError> {
        do {
            let parsedJSON = try JSONDecoder().decode(Forecast.self, from: data)
            return checkResponseValue(parsedJSON)
        } catch {
            fatalError("FATAL ERROR: Parsing failed! Check the response JSON!")
        }
    }
}
