//
//  API.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

enum GenericAPIError: Error {
    case parsingFailed
    case serverError(message: String)
}

class API {

    var session: URLSession!

    init(with sessionConfig: URLSessionConfiguration = URLSessionConfiguration.default) {
        sessionConfig.timeoutIntervalForRequest = 10.0
        sessionConfig.timeoutIntervalForResource = 30.0
        session = URLSession(configuration: sessionConfig)
    }

    func perform(request: URLRequest, withCompletion completion: @escaping (Result<Data, GenericAPIError>) -> ()) {

        let task = session.dataTask(with: request) { (data, response, error) in

            let response = response as? HTTPURLResponse
            guard let statusCode = response?.statusCode else {
                fatalError("FATAL ERROR: API responded without a status code")
            }

            if let unwrappedData = data, (200 ..< 300) ~= statusCode {

                // There was data and the status code is in the right range, well done!
                completion(Result.success(unwrappedData))
            } else {

                // There was no data, an error occurred
                completion(Result.failure(GenericAPIError.serverError(message: "An API error occured with status code \(statusCode)")))
            }
        }

        task.resume()

    }
}

extension API {

    func checkResponseValue<T>(_ parsedValue: T?) -> Result<T, GenericAPIError> {

        guard let parsedItem = parsedValue else {
            return Result.failure(.parsingFailed)
        }

        return Result.success(parsedItem)
    }
}
