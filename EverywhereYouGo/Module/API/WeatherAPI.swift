//
//  WeatherAPI.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

final class WeatherAPI: API, WeatherAPIContract {

    func fetchWeather(with coordinate: Coord, withCompletion completion: @escaping (Result<CityWeatherReport, GenericAPIError>) -> ()) {

        let urlString = URLBuilder.buildFetchWeatherURL(with: coordinate)
        let url = URL(string: urlString)

        guard let urlForRequest = url else {
            fatalError("FATAL ERROR: Creating url from urlstring failed in \(self)")
        }

        let urlRequest = URLRequest(url: urlForRequest, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 3)
        
        API().perform(request: urlRequest) { (result) in

            switch result {
            case .success(let data):

                let result = self.constructCityWeatherReport(from: data)
                completion(result)

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func fetchFiveDayForecast(with coordinate: Coord, withCompletion completion: @escaping (Result<Forecast, GenericAPIError>) -> ()) {

        let urlString = URLBuilder.buildFetchForecastURL(with: coordinate)
        let url = URL(string: urlString)

        guard let urlForRequest = url else {
            fatalError("FATAL ERROR: Creating url from urlstring failed in \(self)")
        }

        let urlRequest = URLRequest(url: urlForRequest, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)

        API().perform(request: urlRequest) { (result) in

            switch result {
            case .success(let data):

                let result = self.constructCityWeatherForecast(from: data)
                completion(result)

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
