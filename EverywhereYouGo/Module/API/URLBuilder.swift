//
//  URLBuilder.swift
//  DottCodingChallenge
//
//  Created by George Coleman on 02/03/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import MapKit

struct URLConstants {
    
    static let API_VERSION = 2.5
    static let baseURL = "http://api.openweathermap.org/data/\(API_VERSION)"
    static let API_KEY: String = "c6e381d8c7ff98f0fee43775817cf6ad"
}

class URLBuilder {
    
    private var urlString: String
    private var firstPair: Bool = true
    
    init(with host: String) {
        self.urlString = host
    }
    
    func with(paramKey: String, paramValue: String) -> URLBuilder {
        firstPair ? self.urlString.append("?") : self.urlString.append("&")
        firstPair = false
        urlString.append("\(paramKey)=\(paramValue)")
        return self
    }
    
    func build() -> String {
        return urlString
    }
}

extension URLBuilder {
    
    static func buildFetchWeatherURL(with coordinate: Coord) -> String {

        let url = URLBuilder(with: "\(URLConstants.baseURL)/weather")
            .with(paramKey: "appid", paramValue: URLConstants.API_KEY)
            .with(paramKey: "units", paramValue: User.shared.unit.rawValue)
            .with(paramKey: "lat", paramValue: "\(coordinate.lat)")
            .with(paramKey: "lon", paramValue: "\(coordinate.lon)")
            .build()

        return url
    }

    static func buildFetchForecastURL(with coordinate: Coord) -> String {

        let url = URLBuilder(with: "\(URLConstants.baseURL)/forecast")
            .with(paramKey: "appid", paramValue: URLConstants.API_KEY)
            .with(paramKey: "units", paramValue: User.shared.unit.rawValue)
            .with(paramKey: "lat", paramValue: "\(coordinate.lat)")
            .with(paramKey: "lon", paramValue: "\(coordinate.lon)")
            .build()

        return url
    }

    static func buildWeatherIconURL(with iconString: String) -> String {
        let url = URLBuilder(with: "http://openweathermap.org/img/w/\(iconString).png")
            .build()
        return url
    }

}
