//
//  Router.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

protocol RouterContract {
    func presentOnboardingWelcome()
    func pushOnboardingInitialSetup(from navigationController: UINavigationController)
    func presentRequestLocationOverlay(with authStatus: LocationStatus, over currentVC: InitialSetupViewController)
    func presentWeather()
    func presentAddLocation(from vc: WeatherViewController)
    func pushLocationDetail(from vc: WeatherViewController, with cityWeatherReport: CityWeatherReport)
    func presentSettings(from vc: WeatherViewController)
}

enum Storyboard: String {

    case Onboarding
    case Weather
    case Settings

    var instance: UIStoryboard {
        return UIStoryboard(name: rawValue, bundle: nil)
    }
}

class Router: RouterContract {

    // MARK: - Properties
    private weak var window: UIWindow?

    // MARK: - Initializers
    init(window: UIWindow) {
        self.window = window
    }

}

// MARK: Onboarding
extension Router {

    func presentOnboardingWelcome() {
        let welcomeViewController = WelcomeViewController.create(from: .Onboarding)
        welcomeViewController.router = self
        let navigationController = UINavigationController(rootViewController: welcomeViewController)
        navigationController.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = navigationController
    }

    func pushOnboardingInitialSetup(from navigationController: UINavigationController) {
        let viewController = InitialSetupViewController.create(from: .Onboarding)
        viewController.router = self
        navigationController.pushViewController(viewController, animated: true)
    }

    func presentRequestLocationOverlay(with authStatus: LocationStatus, over currentVC: InitialSetupViewController) {

        let alertOverlay: LocationPermissionOverlayViewController

        if authStatus == .NoAccess {
            alertOverlay = LocationDeniedOverlayViewController(nibName: "LocationPermissionOverlay", bundle: nil)
        } else {
            alertOverlay = LocationUndeterminedOverlayViewController(nibName: "LocationPermissionOverlay", bundle: nil)
        }

        alertOverlay.delegate = currentVC
        alertOverlay.transitioningDelegate = alertOverlay
        alertOverlay.modalPresentationStyle = .overCurrentContext
        currentVC.present(alertOverlay, animated: true, completion: nil)
    }
}

// MARK: Weather
extension Router {
    func presentWeather() {
        let weatherViewController = WeatherViewController.create(from: .Weather)
        weatherViewController.router = self
        let navigationController = UINavigationController(rootViewController: weatherViewController)
        navigationController.navigationBar.tintColor = UIColor(named: "Black")
        navigationController.navigationBar.barTintColor = UIColor(named: "Orange")
        self.window?.rootViewController = navigationController
    }

    func presentAddLocation(from vc: WeatherViewController) {
        let weatherViewController = WeatherAddLocationViewController.create(from: .Weather) as! WeatherAddLocationViewController
        weatherViewController.delegate = vc
        vc.present(weatherViewController, animated: true, completion: nil)
    }

    func pushLocationDetail(from vc: WeatherViewController, with cityWeatherReport: CityWeatherReport) {
        let weatherDetailViewController = WeatherDetailViewController.create(from: .Weather) as! WeatherDetailViewController
        weatherDetailViewController.cityWeatherReport = cityWeatherReport
        vc.navigationController?.navigationBar.barTintColor = UIColor(named: "Blue")
        vc.navigationController?.pushViewController(weatherDetailViewController, animated: true)
    }
}

// MARK: Settings
extension Router {
    func presentSettings(from vc: WeatherViewController) {
        let weatherViewController = SettingsViewController.create(from: .Settings) as! SettingsViewController
        weatherViewController.delegate = vc
        vc.present(weatherViewController, animated: true, completion: nil)
    }
}
