//
//  AppController.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 18/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import UIKit

final class AppController {

    private let ANIMATION_DURATION = 0.25
    private let window: UIWindow
    private var router: RouterContract!

    init(application: UIApplication, options: [UIApplication.LaunchOptionsKey: Any]?) {
        window = UIWindow(frame: UIScreen.main.bounds)
    }

    func start() -> UIWindow {

        router = Router(window: window)

        User.shared.readUnitsFromDisc()

        if User.shared.readReportsFromDisc() {
            router.presentWeather()
        } else {
            router.presentOnboardingWelcome()
        }

        return window
    }

}
