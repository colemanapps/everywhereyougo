//
//  LocationProviderTests.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import XCTest
import MapKit

@testable import EverywhereYouGo

class LocationViewModelTests: XCTestCase {

    private var viewModel: LocationViewModelContract!

    override func setUp() {
        viewModel = LocationViewModel(with: MockWeatherAPI())
    }

    override func tearDown() {
        viewModel = nil
    }

    func testStateWrongEquatability() {

        let loadingState = State.loading
        let resultsState = State.results

        if loadingState != resultsState {
            XCTAssert(true)
        }
    }

    func testStateEqualibilityLoading() {

        let loadingState = State.loading
        let loadingState2 = State.loading
        if loadingState == loadingState2 {
            XCTAssert(true)
        }
    }

    func testStateEquatabilityResults() {

        let resultsState = State.results
        let resultsState2 = State.results

        if resultsState == resultsState2 {
            XCTAssert(true)
        }
    }

    func testStateErrorEquatability() {

        let errorState = State.error(GenericAPIError.parsingFailed)
        let errorState2 = State.error(GenericAPIError.parsingFailed)

        if errorState == errorState2 {
            XCTAssert(true)
        }
    }
}
