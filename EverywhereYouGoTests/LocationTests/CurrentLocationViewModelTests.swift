//
//  CurrentLocationViewModelTests.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

import XCTest
import MapKit

@testable import EverywhereYouGo

class CurrentLocationViewModelTests: XCTestCase {

    private var viewModel: CurrentLocationViewModelContract!

    override func setUp() {
        viewModel = CurrentLocationViewModel(with: MockUserLocation(), and: MockWeatherAPI())
    }

    override func tearDown() {
        viewModel = nil
    }

    func testGetLocationStatusForAuthAlways() {

        let authStatus = CLAuthorizationStatus.authorizedAlways
        let dottStatus = viewModel.getLocationStatus(for: authStatus)

        XCTAssert(dottStatus == .Granted)
    }

    func testGetLocationStatusForAuthWhenInUse() {

        let authStatus = CLAuthorizationStatus.authorizedWhenInUse
        let dottStatus = viewModel.getLocationStatus(for: authStatus)

        XCTAssert(dottStatus == .Granted)
    }

    func testGetLocationStatusForAuthWhenNotDetermined() {

        let authStatus = CLAuthorizationStatus.notDetermined
        let dottStatus = viewModel.getLocationStatus(for: authStatus)

        XCTAssert(dottStatus == .NotDetermined)
    }

    func testGetLocationStatusForAuthWhenDenied() {

        let authStatus = CLAuthorizationStatus.denied
        let dottStatus = viewModel.getLocationStatus(for: authStatus)

        XCTAssert(dottStatus == .NoAccess)
    }

    func testGetLocationStatusForDisabledServices() {

        let dottStatus = viewModel.getLocationStatusForDisabledServices()
        XCTAssert(dottStatus == .NoAccess)
    }

    func testGetUserLocation() throws {

        let mockProvider = viewModel.locationService as! MockUserLocation

        mockProvider.foundLocation = Locations.Amsterdam
        viewModel.requestUserLocation()

        XCTAssertEqual(viewModel.locationService.foundLocation?.lat, Locations.Amsterdam.lat)
        XCTAssertEqual(viewModel.locationService.foundLocation?.lon, Locations.Amsterdam.lon)
    }

    func testGetUserLocationWithError() throws {

        let mockProvider = viewModel.locationService as! MockUserLocation

        expectFatalError(expectedMessage: "FATAL ERROR: Found location didSet called with nil, should not happen!") {
            mockProvider.foundLocation = nil
        }
    }
}
