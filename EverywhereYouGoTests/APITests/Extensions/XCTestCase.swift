//
//  XCTestCase.swift
//  LocoLogInTests
//
//  Created by George Coleman on 14/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//
// BASED ON: https://medium.com/@marcosantadev/how-to-test-fatalerror-in-swift-e1be9ff11a29

import Foundation
import XCTest

@testable import EverywhereYouGo

extension XCTestCase {
    func expectFatalError(expectedMessage: String, testcase: @escaping () -> Void) {

        // arrange
        let expectation = self.expectation(description: "expectingFatalError")
        var assertionMessage: String? = nil

        // override fatalError. This will pause forever when fatalError is called.
        FatalErrorUtil.replaceFatalError { message, _, _ in
            assertionMessage = message
            expectation.fulfill()
            unreachable()
        }

        // act, perform on separate thead because a call to fatalError pauses forever
        DispatchQueue.global(qos: .userInitiated).async(execute: testcase)

        waitForExpectations(timeout: 0.1) { _ in
            // assert
            XCTAssertEqual(assertionMessage, expectedMessage)

            // clean up
            FatalErrorUtil.restoreFatalError()
        }
    }
}
