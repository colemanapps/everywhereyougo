//
//  APITests.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

import XCTest

@testable import EverywhereYouGo

class APITests: XCTestCase {

    private var mockAPI: API!
    private var liveAPI: API!

    struct MockJSONFiles {
        static let mockResponse = "mock_response"
    }

    override func setUp() {
        self.liveAPI = API()

        let mockSessionConfiguration = URLSessionConfiguration.ephemeral
        mockSessionConfiguration.protocolClasses = [MockURLProtocol.self]
        self.mockAPI = API(with: mockSessionConfiguration)
    }

    override func tearDown() {
        self.liveAPI = nil
        self.mockAPI = nil
    }

    func testLiveRequest() {
        let expect = expectation(description: "get request with live")

        liveAPI.perform(request: URLRequest(url: URL(string: "https://httpbin.org/get")!)) { (result) in
            expect.fulfill()
        }

        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testMockRequest() {

        let expect = expectation(description: "get request with mock")

        mockAPI.perform(request: URLRequest(url:URL(string: MockJSONFiles.mockResponse)!)) { (result) in
            expect.fulfill()
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testResultSuccess() {

        let expect = expectation(description: "test successful response")

        mockAPI.perform(request: URLRequest(url:URL(string: MockJSONFiles.mockResponse)!)) { (result) in
            switch result {
            case .success(_):
                expect.fulfill()
            case .failure(_):
                print("Test result success failed")
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testResponseDataError() {

        let expect = expectation(description: "test failed response")

        liveAPI.perform(request: URLRequest(url: URL(string: "https://httpbin.org/status/404")!)) { (result) in
            switch result {
            case .success(_):
                print("Test 404 failed")
            case .failure(_):
                expect.fulfill()
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testResponseWithoutStatusCode() {
        expectFatalError(expectedMessage: "FATAL ERROR: API responded without a status code") {
            self.liveAPI.perform(request: URLRequest(url: URL(string: "apple")!)) { (_) in
            }
        }
    }

    func testCheckResponseValueWithNil() {

        let nilValue: String? = nil

        let response = mockAPI.checkResponseValue(nilValue)

        switch response {
        case .success(_):
            print("Test nil in response value failed")
        case .failure(let err):
            if case GenericAPIError.parsingFailed = err {
                XCTAssert(true)
            } else {
                XCTAssert(false)
            }
        }
    }

    func testCheckResponseValueWithValue() {

        let nonNilValue: String = "test_string"

        let response = mockAPI.checkResponseValue(nonNilValue)

        switch response {
        case .success(_):
            XCTAssert(true)
        case .failure(_):
            XCTAssert(false)
        }
    }
}
