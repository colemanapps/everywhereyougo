//
//  MockUserLocation.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

@testable import EverywhereYouGo

class MockUserLocation: UserLocationContract {

    var delegate: UserLocationDelegate?

    var foundLocation: Coord? {
        didSet {
            guard let coordinate = foundLocation else { fatalError("FATAL ERROR: Found location didSet called with nil, should not happen!") }
            delegate?.locationAreaFound(with: coordinate)
        }
    }

    func requestUserLocation() {
        foundLocation = Locations.Amsterdam
    }
}
