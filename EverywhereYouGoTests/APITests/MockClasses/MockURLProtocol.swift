//
//  MockURLProtocol.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

@testable import EverywhereYouGo

class MockURLProtocol: URLProtocol {
    private let cannedHeaders = ["Content-Type" : "application/json; charset=utf-8"]

    // MARK: Properties
    private struct PropertyKeys {
        static let handledByForwarderURLProtocol = "HandledByProxyURLProtocol"
    }

    lazy var session: URLSession = {
        let configuration: URLSessionConfiguration = {
            let configuration = URLSessionConfiguration.ephemeral
            return configuration
        }()
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        return session
    }()

    // MARK: Class Request Methods
    override class func canInit(with request: URLRequest) -> Bool {
        return URLProtocol.property(forKey: PropertyKeys.handledByForwarderURLProtocol, in: request) == nil
    }

    override class func requestIsCacheEquivalent(_ a: URLRequest, to b: URLRequest) -> Bool {
        return false
    }

    // MARK: Loading Methods
    override func startLoading() {
        if let data = MockURLProtocol.getDataFile(for: request),
            let url = request.url,
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: cannedHeaders) {
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: URLCache.StoragePolicy.notAllowed)
            client?.urlProtocol(self, didLoad: data)
        }
        client?.urlProtocolDidFinishLoading(self)
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override func stopLoading() {
    }

    static func getDataFile(for request: URLRequest? = nil, or pathName: String? = nil) -> Data? {

        let path: String

        if let pathName = pathName {
            path = pathName
        } else if let parts = (request?.url?.pathComponents), let pathName = parts.suffix(1).first {
            path = pathName
        } else {
            return nil
        }

        print("MOCKING RESPONSE WITH JSON: \(path).json")

        let testBundle = Bundle(for: APITests.self)
        if let path = testBundle.path(forResource: "\(path)", ofType: "json") {
            do {
                return try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            }
        }

        return nil
    }
}

// MARK: NSURLSessionDelegate extension
extension MockURLProtocol: URLSessionDelegate {
    func URLSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceiveData data: Data) {
        client?.urlProtocol(self, didLoad: data)
    }

    func URLSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let response = task.response {
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        }
        client?.urlProtocolDidFinishLoading(self)
    }
}
