//
//  MockUser.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation

@testable import EverywhereYouGo

class MockUser: UserContract {

    var weatherReports: [CityWeatherReport] = []

//    // if the weather report already exists, it's a refresh. Replace it with the newer version.
//    func addWeatherReport(with cityWeatherReport: CityWeatherReport) {
//
//        var valueWasAdded = false
//        for (idx, report) in weatherReports.enumerated() {
//            if report.coordinate.lat == cityWeatherReport.coordinate.lat && report.coordinate.lon == cityWeatherReport.coordinate.lon {
//                weatherReports[idx] = cityWeatherReport
//                valueWasAdded = true
//            }
//        }
//
//        // if the value wasn't added in the for loop it's a new entry, append it!
//        if !valueWasAdded {
//
//            // For now, don't add locations with the same name.
//            // TODO: Make this a little bit smarter to be sure that it's not vastly different cities that happen to share a name in another country for example
//            guard !weatherReports.contains(where: { $0.name == cityWeatherReport.name }) else { return }
//            weatherReports.append(cityWeatherReport)
//        }
//    }

    func remove(at coord: Coord) {
        let weatherReportArray = self.weatherReports.filter({ !($0.coordinate.lat == coord.lat && $0.coordinate.lon == coord.lon) })
        self.weatherReports = weatherReportArray
    }

//    func remove(with names: [String]) {
//        let weatherReportArray = self.weatherReports.filter({ !(names.contains($0.name)) })
//        self.weatherReports = weatherReportArray
//    }
}
