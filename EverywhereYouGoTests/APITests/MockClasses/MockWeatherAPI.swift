//
//  MockWeatherAPI.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import Foundation
import MapKit

@testable import EverywhereYouGo

struct Locations {
    static let Amsterdam = Coord(lat: 52.35163455094293, lon: 4.8971820473607295)
    static let Invalid = Coord(lat: -1.000, lon: -1.000)
}

class MockWeatherAPI: API, WeatherAPIContract {

    private var mockAPI: API {
        let mockSessionConfiguration = URLSessionConfiguration.ephemeral
        mockSessionConfiguration.protocolClasses = [MockURLProtocol.self]
        return API(with: mockSessionConfiguration)
    }

    struct MockJSONFiles {
        static let amsterdamWeatherReport = "weather_report_amsterdam"
    }

    func fetchWeather(with coordinate: Coord, withCompletion completion: @escaping (Result<CityWeatherReport, GenericAPIError>) -> ()) {

        if coordinate.lat == Locations.Invalid.lat && coordinate.lon == Locations.Invalid.lon {

            let nilValue: CityWeatherReport? = nil

            let result = mockAPI.checkResponseValue(nilValue)
            completion(result)

        } else {

            let data = MockURLProtocol.getDataFile(or: MockJSONFiles.amsterdamWeatherReport)!
            let parsedJSON = try! JSONDecoder().decode(CityWeatherReport.self, from: data)
            let result = checkResponseValue(parsedJSON)

            completion(result)
        }
    }

    func fetchFiveDayForecast(with coordinate: Coord, withCompletion completion: @escaping (Result<Forecast, GenericAPIError>) -> ()) {

        if coordinate.lat == Locations.Invalid.lat && coordinate.lon == Locations.Invalid.lon {

            let nilValue: Forecast? = nil

            let result = mockAPI.checkResponseValue(nilValue)
            completion(result)

        } else {

            let data = MockURLProtocol.getDataFile(or: MockJSONFiles.amsterdamWeatherReport)!
            let parsedJSON = try! JSONDecoder().decode(Forecast.self, from: data)
            let result = checkResponseValue(parsedJSON)

            completion(result)
        }
    }
}
