//
//  MapAnnotationProviderTests.swift
//  EverywhereYouGo
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import XCTest
import MapKit

@testable import EverywhereYouGo

class MapAnnotationProviderTests: XCTestCase {

    private var viewModel: MapViewModel!
    private var firstAnnotation: MKAnnotation?
    private var annotations: [MKPointAnnotation]!
    private var annotationsArrayWithThreeTimesAmsterdam: [MKPointAnnotation]!

    override func setUp() {
        viewModel = MapViewModel(with: MapAnnotationProvider(), and: MockWeatherAPI())
        viewModel.fetchWeather(with: Locations.Amsterdam)

        annotations = viewModel.annotationProvider.createMapAnnotations(for: [viewModel!.dataSource])
        annotationsArrayWithThreeTimesAmsterdam = viewModel.annotationProvider.createMapAnnotations(for: [viewModel!.dataSource, viewModel!.dataSource, viewModel!.dataSource])

        firstAnnotation = annotations.first
    }

    override func tearDown() {
        viewModel = nil
        firstAnnotation = nil
        annotations = nil
        annotationsArrayWithThreeTimesAmsterdam = nil
    }

    func testCreateMapAnnotations() {
        XCTAssertEqual(1, annotations.count)
    }

    func testSetCurrentlySelectedAnnotation() {

        viewModel.annotationProvider.setCurrentlySelectedAnnotation(annotation: firstAnnotation)

        XCTAssertEqual(viewModel.annotationProvider.currentlySelectedAnnotation?.title, firstAnnotation?.title)

    }

    func testRemoveMapAnnotations() {

        let annotationArray = viewModel.annotationProvider.removeMapAnnotations(for: viewModel!.dataSource)
        XCTAssertEqual(0, annotationArray.count)
    }

    func testGetUniqueAnnotations() {

        let annotationArray = viewModel.annotationProvider.getUniqueAnnotations(from: annotationsArrayWithThreeTimesAmsterdam)
        XCTAssertEqual(0, annotationArray.count)

    }


    func testMapViewViewForAnnotation() {
        let rect = CGRect(x: 0, y: 0, width: 100, height: 100)
        let annotationView = viewModel.annotationProvider.mapView(MKMapView(frame: rect), viewFor: firstAnnotation!, with: UIButton(frame: rect))

        XCTAssertNotNil(annotationView)

    }}
