//
//  MapViewModelTests.swift
//  EverywhereYouGoTests
//
//  Created by George Coleman on 21/06/2019.
//  Copyright © 2019 Coleman Apps. All rights reserved.
//

import XCTest
import MapKit

@testable import EverywhereYouGo

class MapViewModelTests: XCTestCase {

    private var viewModel: MapViewModel!
    private var annotations: [MKPointAnnotation]!
    private let user = User.shared

    override func setUp() {
        viewModel = MapViewModel(with: MapAnnotationProvider(), and: MockWeatherAPI(), and: MockUser())
        viewModel.fetchWeather(with: Locations.Amsterdam)
    }

    override func tearDown() {
        viewModel = nil
        annotations = nil
    }

    func testCreateMapAnnotations() {
        let value = viewModel.createMapAnnotations(for: [viewModel!.dataSource])
        XCTAssertEqual(1, value.count)
    }

    func testRemoveCity() {
        viewModel.user.weatherReports.append(viewModel.dataSource)
        let annotation = viewModel.createMapAnnotations(for: [viewModel!.dataSource]).first!
        viewModel.annotationProvider.currentlySelectedAnnotation = annotation
        viewModel.removeCity()
        XCTAssertEqual(0, viewModel.user.weatherReports.count)
    }
}
